function sumar(x: number, y:number): number {
    return x+y;
}

function restar(x: number, y:number): number {
    return x-y;
}

function multiplicar(x: number, y:number): number {
    return x*y;
}

function dividir(x: number, y:number): number {
    if (y === 0){
        throw new Error('Divisor no puede ser cero');
    }
    return x/y;
}

export { sumar, restar, multiplicar, dividir };