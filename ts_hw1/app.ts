import {sumar,
        restar, 
        multiplicar as mult, 
        dividir} from './calculadora.1';

import * as operaciones from './gestion-operadores';
import { Operador } from './model/operador';
import { Operaciones } from './operaciones';
import { IOperador } from './model/ioperador';

//let operadores:Array<Operador> = [];
const operadores:Array<IOperador> = [];

const juan = new Operador('Juan', 10, 1);
const maria = new Operador('Maria', 20, 1);
operadores.push(juan);
operadores.push(maria);
console.log('La suma es: ', maria.calcular(Operaciones.sumar, 12, 2));

let res = maria.calcularV2(3,7, (x,y) => {
    return x*y;
});
console.log('La multiplicación es: ', res);



let sandra:IOperador = {
    nombre:'Sandra',
    edad: 2,
    categoria: 1,
    plantilla: true,
    calcular:function(t:number, o1:number, o2:number):number {
        return o1*o2;
    }
}



//const operadores:string[] = ['juan', 'maria'];



/* operadores = operaciones.alta(new Operador('mario', 50, 2), operadores);
operaciones.leerOperadores(operadores);
operadores = operaciones.baja('juan', operadores);
operaciones.leerOperadores(operadores);
operadores.forEach(function(item){
    console.log(item);
}); */

console.log('Calculadora');

let resultado:number = sumar(2,2);
console.log('Resultado de la suma:', resultado);

resultado = restar(2,2);
console.log('Resultado de la resta:', resultado);

resultado = mult(2,2);
console.log('Resultado de la multiplicación:', resultado);

try {
    let test:string = "A";
    resultado = dividir(3,2);
    console.log('Resultado de la división:', resultado, test);
} catch (error){
    console.log(`${error}`);
}
