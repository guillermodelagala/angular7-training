"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Persona {
    get nombre() {
        return this._nombre;
    }
    get edad() {
        return this._edad;
    }
    constructor(nombre, edad) {
        this._nombre = nombre;
        this._edad = edad;
    }
}
exports.Persona = Persona;
