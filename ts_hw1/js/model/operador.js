"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const operaciones_1 = require("../operaciones");
const persona_1 = require("./persona");
const calculadora_1_1 = require("../calculadora.1");
class Operador extends persona_1.Persona {
    constructor(nombre, edad, categoria) {
        super(nombre, edad);
        this.calcular = (tipoOperacion = operaciones_1.Operaciones.sumar, op1, op2) => {
            let resultado = 0;
            switch (tipoOperacion) {
                case operaciones_1.Operaciones.sumar:
                    resultado = calculadora_1_1.sumar(op1, op2);
                    break;
                case operaciones_1.Operaciones.restar:
                    resultado = calculadora_1_1.restar(op1, op2);
                    break;
                case operaciones_1.Operaciones.multiplicar:
                    resultado = calculadora_1_1.multiplicar(op1, op2);
                    break;
                case operaciones_1.Operaciones.dividir:
                    resultado = calculadora_1_1.dividir(op1, op2);
                    break;
                default:
            }
            return resultado;
        };
        this._categoria = categoria;
    }
    get categoria() {
        return this._categoria;
    }
    calcularV2(op1, op2, calc) {
        return calc(op1, op2);
    }
}
exports.Operador = Operador;
