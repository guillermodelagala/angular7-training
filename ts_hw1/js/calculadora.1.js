"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function sumar(x, y) {
    return x + y;
}
exports.sumar = sumar;
function restar(x, y) {
    return x - y;
}
exports.restar = restar;
function multiplicar(x, y) {
    return x * y;
}
exports.multiplicar = multiplicar;
function dividir(x, y) {
    if (y === 0) {
        throw new Error('Divisor no puede ser cero');
    }
    return x / y;
}
exports.dividir = dividir;
