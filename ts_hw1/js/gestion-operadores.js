"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function alta(operador, lista) {
    //return lista.push(operador) ? true : false;
    return [...lista, operador]; //COPIA o CLONACION DE LA LISTA original más el valor añadido
}
exports.alta = alta;
function baja(operador, lista) {
    let nuevaLista = [];
    try {
        nuevaLista = lista.filter(function (item) {
            return item.nombre.toLowerCase() !== operador.toLowerCase();
        });
    }
    catch (error) {
    }
    return nuevaLista;
}
exports.baja = baja;
function leerOperadores(lista = []) {
    /* for (let i=0; i<lista.length; i++){
        console.log(`Nombre operador: ${lista[i]}`);
    } */
    /* for(let [i, operador] of lista.entries()) {
        console.log(`Nombre operador: ${operador.nombre}, ${i}`);
    } */
    for (let operador of lista) {
        console.log(`Nombre operador: ${operador.nombre}`);
    }
    console.log("**************");
}
exports.leerOperadores = leerOperadores;
