"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Operaciones;
(function (Operaciones) {
    Operaciones[Operaciones["sumar"] = 0] = "sumar";
    Operaciones[Operaciones["restar"] = 1] = "restar";
    Operaciones[Operaciones["multiplicar"] = 2] = "multiplicar";
    Operaciones[Operaciones["dividir"] = 3] = "dividir";
})(Operaciones = exports.Operaciones || (exports.Operaciones = {}));
