"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const calculadora_1_1 = require("./calculadora.1");
const operador_1 = require("./model/operador");
const operaciones_1 = require("./operaciones");
//let operadores:Array<Operador> = [];
const operadores = [];
const juan = new operador_1.Operador('Juan', 10, 1);
const maria = new operador_1.Operador('Maria', 20, 1);
operadores.push(juan);
operadores.push(maria);
console.log('La suma es: ', maria.calcular(operaciones_1.Operaciones.sumar, 12, 2));
let res = maria.calcularV2(3, 7, (x, y) => {
    return x * y;
});
console.log('La multiplicación es: ', res);
let sandra = {
    nombre: 'Sandra',
    edad: 2,
    categoria: 1,
    plantilla: true,
    calcular: function (t, o1, o2) {
        return o1 * o2;
    }
};
//const operadores:string[] = ['juan', 'maria'];
/* operadores = operaciones.alta(new Operador('mario', 50, 2), operadores);
operaciones.leerOperadores(operadores);
operadores = operaciones.baja('juan', operadores);
operaciones.leerOperadores(operadores);
operadores.forEach(function(item){
    console.log(item);
}); */
console.log('Calculadora');
let resultado = calculadora_1_1.sumar(2, 2);
console.log('Resultado de la suma:', resultado);
resultado = calculadora_1_1.restar(2, 2);
console.log('Resultado de la resta:', resultado);
resultado = calculadora_1_1.multiplicar(2, 2);
console.log('Resultado de la multiplicación:', resultado);
try {
    let test = "A";
    resultado = calculadora_1_1.dividir(3, 2);
    console.log('Resultado de la división:', resultado, test);
}
catch (error) {
    console.log(`${error}`);
}
