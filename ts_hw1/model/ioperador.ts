import { Operaciones } from "../operaciones";

export interface IOperador {
    nombre:string;
    edad:number;
    categoria:number;
    plantilla?:boolean;
    calcular(tipoOperacion:Operaciones, op1:number, op2:number):number;
}