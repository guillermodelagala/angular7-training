export class Persona {
    private _nombre:string;
    private _edad:number;

    get nombre(){
        return this._nombre;
    }

    get edad(){
        return this._edad;
    }

    constructor(nombre:string, edad:number){
        this._nombre = nombre;
        this._edad = edad;
    }
}