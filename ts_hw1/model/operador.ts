import { Operaciones } from "../operaciones";
import { IOperador } from './ioperador';
import { Persona } from "./persona";
import {sumar,
        restar, 
        multiplicar as mult, 
        dividir} from '../calculadora.1';

export class Operador extends Persona implements IOperador{
    private _categoria:number;

    get categoria(){
        return this._categoria;
    }

    constructor(nombre:string, edad:number, categoria:number){
        super(nombre, edad);
        this._categoria = categoria;
    }

    calcular = (tipoOperacion: Operaciones = Operaciones.sumar, op1:number, op2:number):number => {
        let resultado = 0;
        switch(tipoOperacion) {
            case Operaciones.sumar:
                resultado = sumar(op1, op2);
                break;
            case Operaciones.restar:
                resultado = restar(op1, op2);
                break;
            case Operaciones.multiplicar:
                resultado = mult(op1, op2);
                break;
            case Operaciones.dividir:
                resultado = dividir(op1, op2);
                break;
            default:
        }
        return resultado;
    }

    calcularV2(op1:number, op2:number, calc:(x:number, y:number) => number){
        return calc(op1,op2);
    }
}