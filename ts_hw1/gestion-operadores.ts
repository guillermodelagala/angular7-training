import { Operador } from "./model/operador";

function alta (operador:Operador, lista:Array<Operador>):Operador[] {
    //return lista.push(operador) ? true : false;
    return [...lista, operador] //COPIA o CLONACION DE LA LISTA original más el valor añadido
}

function baja (operador:string, lista:Array<Operador>):Array<Operador> {
    let nuevaLista:Operador[] = [];    
    try {
        nuevaLista = lista.filter(function(item:Operador){
            return item.nombre.toLowerCase() !== operador.toLowerCase();
        });
        
    } catch (error) {
    }
    return nuevaLista;
}

function leerOperadores(lista:Operador[] = []) {
    /* for (let i=0; i<lista.length; i++){
        console.log(`Nombre operador: ${lista[i]}`);
    } */
    /* for(let [i, operador] of lista.entries()) {
        console.log(`Nombre operador: ${operador.nombre}, ${i}`);
    } */
    for(let operador of lista) {
        console.log(`Nombre operador: ${operador.nombre}`);
    }
    console.log("**************");
}

export {alta, baja, leerOperadores};