export default function sumar(x: number, y:number): number {
    return x+y;
}

export function restar(x: number, y:number): number {
    return x-y;
}

export function multiplicar(x: number, y:number): number {
    return x*y;
}

export function dividir(x: number, y:number): number {
    if (y === 0){
        throw new Error('Divisor no puede ser cero');
    }
    return x/y;
}