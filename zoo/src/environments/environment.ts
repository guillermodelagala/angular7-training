// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // Initialize Firebase
  firebaseConfig: {
    apiKey: 'AIzaSyDgqRSZehWlG7s6xe7uMuLxkiKpniQaKEs',
    authDomain: 'angular7-zoo.firebaseapp.com',
    databaseURL: 'https://angular7-zoo.firebaseio.com',
    projectId: 'angular7-zoo',
    storageBucket: 'angular7-zoo.appspot.com',
    messagingSenderId: '954813358124'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
