import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private angularFireAuth: AngularFireAuth,
    private router: Router
  ) { }

  async loginByGoogle() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.angularFireAuth.auth.signInWithPopup(provider);
    console.log(credential);
  }

  logout() {
    this.angularFireAuth.auth.signOut();
    // TODO Enrutar hacia login/home
    this.router.navigateByUrl('/');
  }
}
