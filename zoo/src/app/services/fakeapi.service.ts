import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Animal } from '../models/animal';
import { Fakephotos } from '../models/fakephotos';

@Injectable({
  providedIn: 'root'
})
export class FakeapiService {

  animalesFake: Array<Fakephotos> = [];

  constructor(
    private httpService: HttpClient
  ) { }

  obtenerFotos(): Observable<Fakephotos[]> {
    return this.httpService.get<Fakephotos[]>('https://jsonplaceholder.typicode.com/photos')
      .pipe(
        tap(data => {
          this.animalesFake = data;
        })
      );
  }
}
