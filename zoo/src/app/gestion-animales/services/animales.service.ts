import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Animal } from 'src/app/models/animal';

@Injectable()
export class AnimalesService {

  constructor(
    private angularFirestore: AngularFirestore
  ) { }

  obtenerAnimales() {
    // return this.angularFirestore.collection('animales').valueChanges();
    return this.angularFirestore.collection('animales').snapshotChanges();
  }

  altaAnimal(animal: Animal) {
    this.angularFirestore.collection('animales').add(animal);
  }

  obtenerAnimal(id: string) {
    return this.angularFirestore.doc('animales/' + id).ref.get(); // NO SE ACTUALIZAN LOS CAMBIOS
  }

  obtenerAnimal_V2(id: string) {
    return this.angularFirestore.collection('animales').doc(id).valueChanges();
  }

  borrarAnimal(id: string) {
    return this.angularFirestore.collection('animales').doc(id).delete();
  }

  editarAnimal = (id: string, animal: Animal) => {
    return this.angularFirestore.collection('animales').doc(id).set(animal, {merge: true});
  }
}
