import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { AnimalesService } from '../../services/animales.service';
import { Animal } from 'src/app/models/animal';

@Component({
  selector: 'app-alta-animales',
  templateUrl: './alta-animales.component.html',
  styleUrls: ['./alta-animales.component.css']
})
export class AltaAnimalesComponent implements OnInit {

  altaForm;
  animal: Animal;

  @Output()
  doLogin: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private animalesService: AnimalesService
  ) { }

  ngOnInit() {
    this.altaForm = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      bio: ['', [Validators.required, Validators.minLength(2)]],
      imagen: '',
      cuidadores: this.formBuilder.array([]),
      raza: this.formBuilder.array([])
    });
  }

  get cuidadoresForm() {
    return this.altaForm.get('cuidadores') as FormArray;
  }

  nuevoCuidador() {
    const cuidador = this.formBuilder.group({
      nombreCuidador: ['', Validators.required]
    });

    this.cuidadoresForm.push(cuidador);
  }

  alta() {
    this.animalesService.altaAnimal(this.altaForm.value);
  }

}
