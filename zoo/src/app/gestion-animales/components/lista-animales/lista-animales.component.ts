import { Component, OnInit, OnDestroy } from '@angular/core';
import { AnimalesService } from '../../services/animales.service';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { FakeapiService } from 'src/app/services/fakeapi.service';
import { Fakephotos } from 'src/app/models/fakephotos';

@Component({
  selector: 'app-lista-animales',
  templateUrl: './lista-animales.component.html',
  styleUrls: ['./lista-animales.component.css']
})
export class ListaAnimalesComponent implements OnInit, OnDestroy {

  private subscripcionAnimales: Subscription;
  public animales: any[];
  public fakephotos: Fakephotos[];

  constructor(
    private animalesService: AnimalesService,
    private translateService: TranslateService,
    private fakeapiService: FakeapiService
  ) {
    this.translateService.setDefaultLang('es');
  }

  ngOnInit() {
    this.subscripcionAnimales = this.animalesService.obtenerAnimales().subscribe(data => {
      this.animales = data;
    });

    this.fakeapiService.obtenerFotos().subscribe(data => {
      this.fakephotos = data;
    });
  }

  ngOnDestroy() {
    this.subscripcionAnimales.unsubscribe();
  }

  borrarAnimal(id: string) {
    this.animalesService.borrarAnimal(id);
  }

}
