import { Component, OnInit } from '@angular/core';
import { AnimalesService } from 'src/app/gestion-animales/services/animales.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-detalle-animales',
  templateUrl: './detalle-animales.component.html',
  styleUrls: ['./detalle-animales.component.css']
})
export class DetalleAnimalesComponent implements OnInit {

  animal;
  animal$;
  idAnimal: string;

  constructor(
    private animalesService: AnimalesService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    // Sin suscribirse a los cambios
    this.route.params.subscribe(params => {
      this.animalesService.obtenerAnimal(params.idanimal).then(data => {
        this.animal = data.exists ? data.data() : null;
      }).catch(err => {
        console.log('Error', err);
      });
    });

    // Con async
    this.animal$ = this.route.params.pipe(
      map(params => params.idanimal),
      switchMap(id => {
        return this.animalesService.obtenerAnimal_V2(id);
      })
    );

    // Subscribiéndote a los cambios con observable anidado
    this.route.params.pipe(
      map(params => params.idanimal),
      tap(id => {
        this.idAnimal = id;
      }),
      switchMap(id => {
        return this.animalesService.obtenerAnimal_V2(id);
      })
    ).subscribe(data => {
      this.animal = data;
    });
  }

  async borrarAnimal() {
    try {
      await this.animalesService.borrarAnimal(this.idAnimal);
      this.router.navigateByUrl('/');
    } catch (error) {
      console.log(error);
    }

    /* Otra forma
    this.animalesService.borrarAnimal(this.animal).then(_ => {
      this.router.navigateByUrl('/');
    }).catch(err => {

    }); */
  }

  editarAnimal() {
    this.animal.nombre = 'Nombre';
    this.animalesService.editarAnimal(this.idAnimal, this.animal).then(_ => {
      this.router.navigateByUrl('/');
    }).catch(error => {
      console.log(error);
    });
  }

}
