import { Directive, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appLupa]'
})
export class LupaDirective {

  constructor(
    private elem: ElementRef
  ) { }

  @HostBinding('width')
  width = 200;

  @HostListener('mouseover')
  ondragover(e) {
    this.width = 400;
  }

  @HostListener('mouseleave')
  ondragout(e) {
    this.width = 200;
  }

}
