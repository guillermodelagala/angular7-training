import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaAnimalesComponent } from './components/lista-animales/lista-animales.component';
import { DetalleAnimalesComponent } from './components/lista-animales/detalle-animales/detalle-animales.component';

const routes: Routes = [
  { path: '',
    children: [
      { path: '', component: ListaAnimalesComponent},
      { path: ':idanimal', component: DetalleAnimalesComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionAnimalesRoutingModule { }
