import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { GestionAnimalesRoutingModule } from './gestion-animales-routing.module';
import { ListaAnimalesComponent } from './components/lista-animales/lista-animales.component';
import { WidgetsModule } from '../widgets/widgets.module';
import { AnimalesService } from './services/animales.service';
import { AltaAnimalesComponent } from './components/alta-animales/alta-animales.component';
import { DetalleAnimalesComponent } from './components/lista-animales/detalle-animales/detalle-animales.component';
import { LupaDirective } from './directives/lupa.directive';


@NgModule({
  declarations: [ListaAnimalesComponent, AltaAnimalesComponent, DetalleAnimalesComponent, LupaDirective],
  providers: [AnimalesService],
  imports: [
    CommonModule,
    WidgetsModule,
    ReactiveFormsModule,
    GestionAnimalesRoutingModule,
    ScrollingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader, // Proveo un servicio pero por detrás uso otro
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http);
        },
        deps: [ HttpClient ]
      }
    }),
  ]
})
export class GestionAnimalesModule { }
