import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'zoo';

  constructor(
    private authService: AuthService,
    private translateService: TranslateService
  ) {
    this.translateService.setDefaultLang('es');
  }

  cambiarIdioma(idIdioma) {
    this.translateService.use(idIdioma);
  }

  login() {
    this.authService.loginByGoogle();
  }

  logout() {
    this.authService.logout();
  }
}
