import { Injectable } from '@angular/core';
import { PersistentService } from '../core/services/persistent.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private persistentService: PersistentService
  ) { }

  login(credentials: any): Promise<boolean> {
    if (credentials.usuario === 'pepe' && credentials.contrasena === 'pepe') {
      this.persistentService.guardarDatos('login', true);
      return Promise.resolve(true);
    } else {
      this.persistentService.guardarDatos('login', false);
      return Promise.resolve(false);
    }
  }
}
