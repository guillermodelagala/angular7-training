import { Injectable } from '@angular/core';
import { Post } from '../model/post';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class PostsService {

  posts: Array<Post> = [];

  constructor(
    private httpService: HttpClient
  ) { }

  obtenerPostsUsuario(userId: string): Observable<Post[]> {
    const params = new HttpParams().set('userId', userId);
    return this.httpService.get<Post[]>('https://jsonplaceholder.typicode.com/posts', {params});
  }
}
