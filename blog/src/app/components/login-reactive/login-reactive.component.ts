import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-reactive',
  templateUrl: './login-reactive.component.html',
  styleUrls: ['./login-reactive.component.css']
})
export class LoginReactiveComponent implements OnInit {
  loginForm;

  @Output()
  doLogin: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usuario: ['User', [Validators.required]],
      contrasena: ['', [Validators.required, Validators.minLength(2)]]
    });
  }

  login() {
    this.authService.login(this.loginForm.value).then(data => {
      this.doLogin.emit(data);
      this.router.navigate(['/usuarios']);
    });
  }

}
