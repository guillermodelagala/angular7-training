import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {
    usuario: '',
    contrasena: ''
  };

  @Output()
  doLogin: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  submitLogin() {
    this.authService.login(this.form).then(data => {
      this.doLogin.emit(data);
      this.router.navigate(['/usuarios']);
    });
    return false;
  }
}
