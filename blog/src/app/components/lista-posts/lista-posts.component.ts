import { Component, OnInit } from '@angular/core';
import { PostsService } from 'src/app/services/posts.service';
import { Post } from 'src/app/model/post';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-posts',
  templateUrl: './lista-posts.component.html',
  styleUrls: ['./lista-posts.component.css'],
  // PostsService sólo se usa en este componente
  providers: [PostsService]
})
export class ListaPostsComponent implements OnInit {

  posts: Post[];

  constructor(
    private postsService: PostsService,
    private router: ActivatedRoute
  ) { }

  ngOnInit() {
    this.router.params.subscribe(param => {
      const idUsuario = param.userid;
      this.postsService.obtenerPostsUsuario(idUsuario).subscribe(data => {
        this.posts = data;
      });
    });
  }

}
