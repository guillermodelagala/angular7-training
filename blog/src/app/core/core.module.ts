import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersistentService } from './services/persistent.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [PersistentService]
})
export class CoreModule { }
