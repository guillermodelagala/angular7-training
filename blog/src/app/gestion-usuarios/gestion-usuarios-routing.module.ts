import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaUsuariosComponent } from './components/lista-usuarios/lista-usuarios.component';
import { AuthGuard } from '../services/auth.guard';
import { UsuarioComponent } from './components/usuario/usuario.component';

const routes: Routes = [
  { path: '',
    children: [
      { path: '', component: ListaUsuariosComponent, canActivate: [AuthGuard] },
      { path: ':userid', component: UsuarioComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionUsuariosRoutingModule { }
