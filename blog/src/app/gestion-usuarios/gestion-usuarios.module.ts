import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GestionUsuariosRoutingModule } from './gestion-usuarios-routing.module';
import { ListaUsuariosComponent } from './components/lista-usuarios/lista-usuarios.component';
import { UsuarioComponent } from './components/usuario/usuario.component';

@NgModule({
  declarations: [
    ListaUsuariosComponent,
    UsuarioComponent
  ],
  imports: [
    CommonModule,
    GestionUsuariosRoutingModule
  ]
})
export class GestionUsuariosModule { }
