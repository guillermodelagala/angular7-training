import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/gestion-usuarios/services/users.service';
import { Observable, Subscription } from 'rxjs';
import { Usuario } from 'src/app/gestion-usuarios/model/usuario';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit, OnDestroy {

  idUsuario: any;
  usuario: Usuario;
  usuarioPush: Usuario[];

  usuarioObservable$: Observable<Usuario>;

  subscripcion: Subscription;

  constructor(
    private route: ActivatedRoute,
    private usuarioService: UsersService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(param => {
      this.idUsuario = param.userid;
      /* Método PUSH */
      this.usuarioPush = this.usuarioService.obtenerUsuarioPush(+this.idUsuario);
      /* Fin método PUSH */

      this.subscripcion = this.usuarioService.obtenerUsuario(+this.idUsuario).subscribe(data => {
        this.usuario = data;
      });

      this.usuarioObservable$ = this.usuarioService.obtenerUsuario(+this.idUsuario);
    });
  }

  ngOnDestroy() {
    this.subscripcion.unsubscribe();
  }

  verListaPosts() {
    this.router.navigate(['/posts', this.idUsuario]);
  }

}
