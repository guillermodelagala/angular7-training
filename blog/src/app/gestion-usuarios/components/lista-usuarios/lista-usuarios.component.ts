import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/gestion-usuarios/services/users.service';
import { Usuario } from 'src/app/gestion-usuarios/model/usuario';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {

  usersPush: Usuario[];
  users: Usuario[];

  constructor(
    private userService: UsersService
  ) { }

  ngOnInit() {
    // Método PUSH
    this.userService.agentePush.subscribe(data => {
      this.usersPush = data;
    });
    this.userService.obtenerUsuariosPush();
    // Fin método PUSH

    this.userService.obtenerUsuarios().subscribe(data => {
      this.users = data;
    });
  }

}
