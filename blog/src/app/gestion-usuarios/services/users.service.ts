import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { Usuario } from '../model/usuario';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  users: Array<Usuario> = [];

  agentePush: Subject<Usuario[]> = new Subject();

  constructor(
    private httpService: HttpClient
  ) {
  }

  obtenerUsuarios(): Observable<Usuario[]> {
    return this.httpService.get<Usuario[]>('https://jsonplaceholder.typicode.com/users')
      .pipe(
        tap(data => {
          this.users = data;
        })
      );
  }

  obtenerUsuario(idUser: number) {
    let user = null;
    try {
      user = this.users.filter((u: Usuario) => {
        return u.id === idUser;
      })[0];
    } catch (error) {
    }
    return of(user);
  }

  /* Método PUSH */
  obtenerUsuariosPush() {
    this.httpService.get<Usuario[]>('https://jsonplaceholder.typicode.com/users').subscribe(
      data => {
        this.users = data;
        this.agentePush.next([...this.users]);
      }
    );
  }

  obtenerUsuarioPush(id: number): Usuario[] {
    return this.users.filter(user => {
        return user.id === id;
    });
  }

  /* FIN MÉTODO PUSH */
}
