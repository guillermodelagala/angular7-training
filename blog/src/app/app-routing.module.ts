import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorComponent } from './components/error/error.component';
import { ListaPostsComponent } from './components/lista-posts/lista-posts.component';

const routes: Routes = [
  { path: 'usuarios', loadChildren: './gestion-usuarios/gestion-usuarios.module#GestionUsuariosModule'}, // Carga lazy del módulo
  { path: '', redirectTo: 'usuarios', pathMatch: 'full' },
  /* { path: 'posts/:userid', component: ListaPostsComponent }, */
  { path: 'posts', children: [
    // Meter varias URL a partir de posts
    { path: ':userid', component: ListaPostsComponent }
  ] },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
