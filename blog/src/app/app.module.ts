import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ErrorComponent } from './components/error/error.component';
import { HttpClientModule } from '@angular/common/http';
import { ListaPostsComponent } from './components/lista-posts/lista-posts.component';
import { CoreModule } from './core/core.module';
import { LoginComponent } from './components/login/login.component';
import { LoginReactiveComponent } from './components/login-reactive/login-reactive.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    ListaPostsComponent,
    LoginComponent,
    LoginReactiveComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
