import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaAlumnoV3Component } from './alta-alumno-v3.component';

describe('AltaAlumnoV3Component', () => {
  let component: AltaAlumnoV3Component;
  let fixture: ComponentFixture<AltaAlumnoV3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaAlumnoV3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaAlumnoV3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
