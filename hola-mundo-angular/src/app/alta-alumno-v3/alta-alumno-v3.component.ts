import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Alumno } from '../model/alumno';
import { AlumnosService } from '../services/alumnos.service';

@Component({
  selector: 'app-alta-alumno-v3',
  templateUrl: './alta-alumno-v3.component.html',
  styleUrls: ['./alta-alumno-v3.component.css']
})
export class AltaAlumnoV3Component implements OnInit {

  user: any = {};

  @Output()
  altaCompletada: EventEmitter<boolean> = new EventEmitter();

  constructor(private alumnosService: AlumnosService) { }

  ngOnInit() {
  }

  async darAlta() {
    console.log(this.user);
    // ¡¡NOTA: es un string y FUNCIONA metiéndolo en el array de numbers de notas!!
    const notas = this.user.notas ? this.user.notas.split(',').map(n => +n) : [];
    const alumno = new Alumno(this.user.nombre, this.user.apellido, this.user.sexo, this.user.avatar, notas);
      // Síncrono
      // this.alumnosService.altaAlumno(alumno);

      // Async - await
      let res = false;
      try {
        res = await this.alumnosService.altaAlumnoAsyncAwait(alumno);
        this.altaCompletada.emit(res);
      } catch (error) {
        console.log('Error en el alta.', error);
        this.altaCompletada.emit(res);
      }
    return false;
  }

}
