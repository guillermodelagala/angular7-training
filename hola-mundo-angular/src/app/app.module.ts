import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ListaAlumnosComponent } from './lista-alumnos/lista-alumnos.component';
import { AltaAlumnoComponent } from './alta-alumno/alta-alumno.component';
import { AlumnoComponent } from './alumno/alumno.component';
import { AltaAlumnoV2Component } from './alta-alumno-v2/alta-alumno-v2.component';
import { AltaAlumnoV3Component } from './alta-alumno-v3/alta-alumno-v3.component';
import { CoreModule } from './core/core.module';
import { MyPipePipe } from './pipes/my-pipe.pipe';

@NgModule({
    declarations: [
        AppComponent,
        ListaAlumnosComponent,
        AltaAlumnoComponent,
        AlumnoComponent,
        AltaAlumnoV2Component,
        AltaAlumnoV3Component,
        MyPipePipe
    ],
    imports: [
        BrowserModule,
        FormsModule,
        CoreModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
