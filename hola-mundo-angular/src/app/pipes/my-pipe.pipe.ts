import { Pipe, PipeTransform } from '@angular/core';
import { Alumno } from '../model/alumno';

@Pipe({
  name: 'myPipe'
})
export class MyPipePipe implements PipeTransform {

  transform(data: Alumno[], origen: number, final: number): Alumno[] {
    // Devolver cierto número de elementos del array
    return data.slice(origen, final);
  }

}
