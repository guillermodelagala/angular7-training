import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaAlumnoV2Component } from './alta-alumno-v2.component';

describe('AltaAlumnoV2Component', () => {
  let component: AltaAlumnoV2Component;
  let fixture: ComponentFixture<AltaAlumnoV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaAlumnoV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaAlumnoV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
