import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alta-alumno-v2',
  templateUrl: './alta-alumno-v2.component.html',
  styleUrls: ['./alta-alumno-v2.component.css']
})
export class AltaAlumnoV2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  darAlta(data) {
    console.log(data[0].value);
    return false;
  }

}
