import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Alumno } from '../model/alumno';
import { pathAvatar } from '../constantes';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html',
  styleUrls: ['./alumno.component.css']
})
export class AlumnoComponent implements OnInit {

  avatar = `${pathAvatar}/`;

  @Input()
  alumno: Alumno;

  @Output()
  calculoMedia: EventEmitter<number[]> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  verMedia() {
    this.calculoMedia.emit(this.alumno.notas);
  }

}
