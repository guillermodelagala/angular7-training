import { Component, OnInit, OnDestroy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Alumno } from '../model/alumno';
import { AlumnosService } from '../services/alumnos.service';
import { PersistService } from '../core/services/persist.service';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-lista-alumnos',
  templateUrl: './lista-alumnos.component.html',
  styleUrls: ['./lista-alumnos.component.css']
})
export class ListaAlumnosComponent implements OnInit, OnDestroy, OnChanges {

  alertMedia: number;
  mediaAlumno: number;

  private alumnos: Alumno[] = [];
  private alumnosAdmitidos: Alumno[] = [];

  subscripcion: Subscription;
  alumnos$: Observable<Alumno[]>;

  @Input()
  notaCorte: number;

  constructor(
    // Inyección de dependencias
    private alumnosService: AlumnosService,
    private persistService: PersistService
  ) { }

  // Se pone async si se usa el método async-await
  async ngOnInit() {
    // Asíncrono
/*     this.alumnosService.obtenerAlumnosAsync()
      .then((data: Alumno[]) => {
        this.alumnos = data;
      })
      .catch(error => {
        console.log(error);
      });

    // Asíncrono 2
    this.alumnosService.obtenerAlumnosAsyncAwait().then((data: Alumno[]) => {
      this.alumnos = data;
    })
    .catch(error => {
      console.log(error);
    });

    // Asíncrono async-await
    this.alumnos = await this.alumnosService.obtenerAlumnosAsyncAwait();

    // Observable:
    this.subscripcion = this.alumnosService.obtenerAlumnosObservable().subscribe(data => {
      this.alumnos = data;
    });

    // Convertir el observable a la promesa
    this.alumnos$ = this.alumnosService.obtenerAlumnosObservable();

    // Observable con next
    this.alumnosService.obtenerAlumnosObservableV2().subscribe(data => {
      console.log(data);
    }); */


    // Primero te subscribes y luego lanzas la inicialización
    this.alumnosService.agentePush.subscribe(data => {
      this.alumnos = data;
    });
    this.alumnosService.obtenerAlumnosAgentePush();

    // Síncrono
    // this.alumnos = this.alumnosService.obtenerAlumnos();

    if (this.notaCorte) {
      // Llamar al servicio
      this.alumnosAdmitidos = this.alumnosService.obtenerAlumnosAdmitidosFilter(this.notaCorte);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Entra también la primera vez
    console.log(changes);
    this.alumnosAdmitidos = this.alumnosService.obtenerAlumnosAdmitidosFilter(this.notaCorte);
  }

  ngOnDestroy() {
    console.log('DESTROY');
    this.persistService.guardarDatos('alumnos', this.alumnos);

    // Quitar la suscripción del observable
    this.subscripcion.unsubscribe();
  }

  verMedia(notas: number[], index: number) {
    this.alertMedia = index;
    this.mediaAlumno = this.alumnosService.obtenerMedia(notas);
    setTimeout(() => {
      this.alertMedia = -1;
    }, 3000);
  }

}
