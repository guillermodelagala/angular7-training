import { Component, ViewChild } from '@angular/core';
import { ListaAlumnosComponent } from './lista-alumnos/lista-alumnos.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    @ViewChild(ListaAlumnosComponent)
    listaAlumnos: any;

    private _titulo = 'mi-mundo';

    get titulo() {
        return this._titulo;
    }

    gestionarAlta(res: boolean) {
        if (res) {
            // Si se hace con NEXT quitar esta línea
            // this.listaAlumnos.ngOnInit();

            // this.listaAlumnos.ngOnDestroy();
        }
    }
}
