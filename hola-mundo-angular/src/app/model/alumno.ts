export enum Sexo {
    hombre,
    mujer
}

export class Alumno {
    private _nombre: string;
    private _apellidos: string;
    private _sexo: number;
    private _notas: number[];
    private _avatar: string;

    get nombre() {
        return this._nombre;
    }

    get apellidos() {
        return this._apellidos;
    }

    get sexo() {
        return this._sexo;
    }

    get notas() {
        return this._notas;
    }

    get avatar() {
        return this._avatar;
    }

    constructor(nombre: string, apellidos: string, sexo: number, avatar: string, notas: number[]) {
        this._nombre = nombre;
        this._apellidos = apellidos;
        this._sexo = sexo;
        this._avatar = avatar;
        this._notas = notas;
    }
}
