import { Injectable } from '@angular/core';
import { Alumno, Sexo } from '../model/alumno';
import { Observable, of, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlumnosService {

  private alumnos: Alumno[] = [];
  private alumnosAdmitidos: Alumno[] = [];

  agentePush: Subject<Alumno[]> = new Subject();

  constructor() {
    this.alumnos.push(new Alumno('Enzo', 'Perez', Sexo.hombre, 'avatarHombre.jpeg', [5.4, 6.7, 3.9]));
    this.alumnos.push(new Alumno('Mario', 'Ortiz', Sexo.hombre, 'avatarHombre.jpeg', [8.4, 3.7, 9.9]));
    this.alumnos.push(new Alumno('Ronaldinha', 'Gaucha', Sexo.mujer, 'avatarMujer.jpeg', [1.4, 8.7, 4.9]));
    this.alumnos.push(new Alumno('David', 'Barral', Sexo.hombre, 'avatarHombre.jpeg', [7.4, 2.7, 0.9]));
  }

  // Agente push primer next
  obtenerAlumnosAgentePush() {
    this.agentePush.next([...this.alumnos]);
  }

  // Síncrono
  altaAlumno = (alumno: Alumno) => {
    try {
      this.alumnos.push(alumno);
    } catch (error) {
      console.log('Ha habido un error al dar de alta: ', error);
    }
  }

  // Asíncrona
  altaAlumnoAsyncAwait = async (alumno: Alumno) => {
    try {
      this.alumnos.push(alumno);

      // Emitir al agente el alta nueva
      this.agentePush.next([...this.alumnos]);
      return true;
    } catch (error) {
      console.log('Ha habido un error al dar de alta: ', error);
      return false;
    }
  }

  // Síncrono
  obtenerAlumnos() {
    // Clonar alumnos para respetar la inmutabilidad
    return [...this.alumnos];
  }

  // Asíncrono
  obtenerAlumnosAsync(): Promise<Alumno[]> {
    return new Promise((resolve, reject) => {
      resolve([...this.alumnos]);
    });
  }

  // Alternativa asíncrona
  async obtenerAlumnosAsyncAwait() {
    return [...this.alumnos];
  }

  // Alternativa con observables
  obtenerAlumnosObservable(): Observable<Alumno[]> {
    return of([...this.alumnos]);
  }

  // Observables con next
  obtenerAlumnosObservableV2() {
    return new Observable(observer => {
      observer.next([...this.alumnos]);
    });
  }

  obtenerAlumnosAdmitidos(notaCorte: number): Alumno[] {
    // Peor forma, usar el filtro
    this.alumnos.forEach(alumno => {
      let totalNotas = 0;
      alumno.notas.forEach(nota => {
        totalNotas += nota;
      });
      if ((totalNotas / alumno.notas.length) >= notaCorte) {
        this.alumnosAdmitidos.push(alumno);
      }
    });
    return this.alumnosAdmitidos;
  }

  obtenerAlumnosAdmitidosFilter(notaCorte: number): Alumno[] {
    this.alumnosAdmitidos = this.alumnos.filter(alumno => {
      return this.obtenerMedia(alumno.notas) >= notaCorte;
    });
    return this.alumnosAdmitidos;
  }

  obtenerMedia(notas: number[]): number {
    let totalNotas = 0;
    for (const nota of notas) {
      totalNotas += nota;
    }
    /* notas.forEach(nota => {
      totalNotas += nota;
    }); */
    return totalNotas / notas.length;
  }

}
