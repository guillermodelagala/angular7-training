import { Injectable } from '@angular/core';

@Injectable()
export class PersistService {

  constructor() { }

  guardarDatos(clave: string, data: any) {
    localStorage.setItem(clave, JSON.stringify(data)); // Serializar data ya que en localStorage sólo van string
  }

  obtenerDatos(clave: string) {
    return JSON.parse(localStorage.getItem(clave));
  }
}
